const gulp = require('gulp');

// gulp plugins and utils
const gutil = require('gulp-util');
const livereload = require('gulp-livereload');
const nodemon = require('gulp-nodemon');
const postcss = require('gulp-postcss');
const sourcemaps = require('gulp-sourcemaps');
const zip = require('gulp-zip');
const imagemin = require('gulp-imagemin');

// postcss plugins
const autoprefixer = require('autoprefixer');
const colorFunction = require('postcss-color-function');
const cssnano = require('cssnano');
const customProperties = require('postcss-custom-properties');
const easyimport = require('postcss-easy-import');

const swallowError = function swallowError(error) {
    gutil.log(error.toString());
    gutil.beep();
    this.emit('end');
};

const nodemonServerInit = () => {
    livereload.listen(1234);
};


gulp.task('css', () => {
    const processors = [
        easyimport,
        customProperties,
        colorFunction(),
        autoprefixer({browsers: ['last 2 versions']}),
        cssnano()
    ];

    return gulp.src('assets/css/*.css')
        .on('error', swallowError)
        .pipe(sourcemaps.init())
        .pipe(postcss(processors))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('assets/built/'))
        .pipe(livereload());
});

gulp.task('images', (done) => {
    gulp.src('assets/images/*')
        .pipe(imagemin())
        .pipe(gulp.dest('assets/images'));
    done();
});

gulp.task('build', gulp.series(gulp.parallel('css', 'images')), () => {
    return nodemonServerInit();
});

gulp.task('watch', () => {
    gulp.watch('assets/css/**', gulp.series('css'));
});

gulp.task('zip', gulp.series(gulp.parallel('css')), () => {
    const targetDir = 'dist/';
    const themeName = require('./package.json').name;
    const filename = themeName + '.zip';

    return gulp.src([
        '**',
        '!node_modules', '!node_modules/**',
        '!dist', '!dist/**'
    ])
        .pipe(zip(filename))
        .pipe(gulp.dest(targetDir));
});

gulp.task('default', gulp.series(gulp.parallel('build')), (done) => {
    gulp.start('watch', done());
});
