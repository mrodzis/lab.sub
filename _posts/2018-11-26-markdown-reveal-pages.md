---
layout: post
current: post
cover: assets/images/Lcars_wallpaper.svg
cover-credits: CC BY-SA 4.0, Ray Koopa from Wikimedia Commons
navigation: True
title: Preparing Presentations with Markdown
date: 2018-11-26 0:00:01
tags: presentation markdown
class: post-template
subclass: 'post tag-presentation'
author: mathias
---

Tired of presentations made with a full-featured (as in: full of features you are never going to use) software suite stored in unhandy proprietary formats? OK, I am sorry for this rhetorical question. But we all are able to create neat presentations
- accessible via any browser,
- fulltext searchable,
- always with a nice touch,
- with the main features we know from heavy software suites, e.g. presenters view or mute the presentation,
- in collaboration with others
- that do not rely on [Google](https://www.google.com/slides/about/).

There are frameworks available that features exactly this. But for using [reveal.js](https://revealjs.com) or [impress.js](https://impress.js.org) you have to setup a JavaScript framework – with a heavy overhead. For example to preview the slides, usually a web-server on your local machine is required and not everyone has to have knowledge of JavaScript or HTML.

Let's strip this down to two technologies: Markdown and git via web-interface. Let's reject JavaScript, HTML, CSS and web-server by combining existing tools: Reveal.js and our computing centers installation of [GitLab](https://gitlab.gwdg.de).

## tl;dr
Use your GWDG account with the following steps to get started:
1. Fork the git repo: [https://gitlab.gwdg.de/mgoebel/slides](https://gitlab.gwdg.de/mgoebel/slides)
2. Edit the file `talks/generic-markdown/index.html` and replace “YOUR TITLE”, “Your Name” and “a very brief description” according to your next talk.
3. Everything else takes place at `talks/generic-markdown/slides.md`. Edit the file and
4. preview your slides at [https://YOUR-GWDG-USERNAME.pages.gwdg.de/slides/talks/generic-markdown/](https://mgoebel.pages.gwdg.de/slides/talks/generic-markdown/)

## Markdown
According to [Wikipedia](https://en.wikipedia.org/wiki/Markdown) Markdown is “a lightweight markup language”. A simple text editor is all we need. Markdown files usually have the file name extension `.md` - like the [one you are reading right now](https://gitlab.gwdg.de/subugoe/lab.sub/blob/master/_posts/2018-11-26-markdown-reveal-pages.md). As you can see here, headings, paragraphs, lists and hyperlinks are pretty much all used to prepare this blog article. Markdown is an informal specification. That is why many descendant formats are available: [CM](https://commonmark.org/), [GFM](https://githubengineering.com/a-formal-spec-for-github-markdown/), [MD Extra](https://en.wikipedia.org/w/index.php?title=Markdown&oldid=869143422#Markdown_Extra).
We are currently on GFM, with a single addition: to use it together with a presentation framework we have to extend the standard as well. A separator is needed to delimit a single slide.

### Markdown in your presentation
It is completely customizable and in this case currently declared as `\n---\n`: three dashes surrounded by empty lines. By using the dashes we benefit from the markdown pattern for a horizontal rule and we get a better impression of what we are doing within the [Markdown Preview GitLab offers on the website](https://gitlab.gwdg.de/mgoebel/slides/blob/master/talks/generic-markdown/slides.md).

Reveal.js offers a two dimensional slide deck: we can setup vertical or horizontal scrolling. The mentioned pattern `\n---\n` is for a horizontal transition, to create a vertical transition use `\n--\n`.  

## GitLab
GitLab offers a web-interface to create files and directories. But the access is restricted and since the username is part of the resulting URL to your slides, it is necessary to create a copy of this repository. In git terms this is called a “fork”.

![gitlab webinterface fork](assets/images/gitlab-repo-fork.png)

Click on the fork button [on this page](https://gitlab.gwdg.de/mgoebel/slides) and select your account in the next step. After this, you have your own git repository with all the files.

### The Repo
What is in there? This repo is prepared with the reveal.js library. It comes with a sample slide deck to show its usage. Besides some more files the following are the important ones:

<pre style="color: black;">
└── <font color="#0087FF">talks</font>
    └── <font color="#0087FF">generic-markdown</font>
        ├── custom.css
        ├── <font color="#0087FF">img</font>
        │   └── <font color="#AD7FA8">some-image.png</font>
        ├── index.html
        └── slides.md
</pre>

## Usage
Whenever a change is made to a file in this git repo (in git terms a “commit”), the website will be recreated and made available via the URL [`https://YOUR-GWDG-USERNAME.pages.gwdg.de/slides/talks/generic-markdown/`](https://YOUR-GWDG-USERNAME.pages.gwdg.de/slides/talks/generic-markdown/). So all we have to do is to write some Markdown in `slides.md` to prepare our slides. There should be an “Edit”-button. ![gitlab edit button](assets/images/gitlab-edit-button.png)
Start committing some changes to this file and browse to the URL named above to see the result. It may takes a few seconds (or up to a minute), before the result is online.

You can upload images to the `img` directory. To include them, the Markdown syntax is ``![Some nice Image](img/some-image.png)``. There are more examples in the sample file.

Last but not least, a good practice is to add very basic metadata. Within `index.html` the title of the talk, the authors name(s) and a very brief description should be added by replacing the corresponding text at the lines [#8ff](https://gitlab.gwdg.de/mgoebel/slides/blob/a8c723b7dab57992cf408adb223dd74ff50b9f4c/talks/generic-markdown/index.html#L8).

The idea is to duplicate the `generic-markdown` directory and name it in accordance to the talk, e.g. `2018-11-27_slides-with-markdown`. The files `index.html` and `slides.md` are mandatory, the others are recommended, but optional. Prepare the directory and the files with the web-interface:

![GitLab create button](assets/images/gitlab-create-button.png)

This way, you can setup `generic-markdown` as your personal template.

## Presentation
Now it is showtime.
We have the slides available on a website which is quite an achievement. To present them we can use a standard browser. The following keyboard commands are considered to be useful:

| key | description |
|-------|------------------------------------------|
| ESC | overview |
| s | speakers view (beware of popup blockers) |
| f | go fullscreen |
| space | next slide |
| b | black screen, mute the presentation |

## Further readings
When you want to add some more layout features, have a look at a [Markdown documentation](https://daringfireball.net/projects/markdown/).
You want to break free? See the [reveal.js documentation](https://github.com/hakimel/reveal.js/blob/c35cce54a5d4800b90e71a43da140c0347308989/README.md#revealjs--) to insert videos, transition effects and more.
