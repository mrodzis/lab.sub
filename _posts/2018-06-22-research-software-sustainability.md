---
layout: post
current: post
cover: assets/images/sustainable.jpg
cover-credits: By Studio Roosegaarde, CC BY 2.0, https://commons.wikimedia.org/w/index.php?curid=16875719
navigation: True
title: Research Software Sustainability
date: 2018-06-22 07:12:00
tags: sustainability
class: post-template
subclass: 'post sustain'
author: mdogan
---
The Alliance of German Science Organisations has recently published its “Recommendations on the Development, Use and Provision of Research Software”, in [English](http://doi.org/10.5281/zenodo.1172988) and [German](http://doi.org/10.5281/zenodo.1172970). Research software has been an emerging subject in recent months, which has been considered as a subtitle of research data by the scientific community for a long time. But software is not data. It must evolve and adapt to the changes in its environment. And if it does not, it decays (s. also this [report](http://repository.jisc.ac.uk/6332/1/Research_Software_Sustainability_Report_on_KE_Workshop_Feb_2016_FINAL.pdf) about research software sustainability).

The recommendations document concentrates on development, usage and provision of research software from the standpoint of different stakeholders, such as developers, infrastructure facilities, senior researchers and research managers and gives a few key recommendations. This is a crucial effort, happily welcome by the research software engineers. But there are still some unaddressed issues:

* Role of data centers and libraries: The document is built upon the assumption that the development of research software takes place only within the framework of research process and is carried out by scientific personnel. The data centers and libraries should fulfill an advisory and archiving role. The possibility that data centers and libraries can offer research software development as a service and can develop software together with researchers for research purposes is not considered as an option. This is what we are trying to build in Gottingen.

* Funding: Research managers are addressed directly by one of the four key recommendations: “Senior researchers and research managers have a special responsibility to set in motion the prerequisites for the long-term development of research software in terms of HR policy and funding, to promote compliance with good scientific practice in relation to the use of software in their institutions and to develop the required funding and business models for the provision of research software.”.  This sounds like a very useful recommendation in theory but very hard to follow in practice. Usually, research software is a by-product of research projects, which are financially supported by funding parties. Research managers do not have the motivation, skills, money or time to fulfill the requirements for the software quality and sustainability – even less so after the funding period. With every new software, the issue gets bigger and gets harder to solve. Without a proper funding scheme and support by research funders, it will not be possible to solve this problem.

* Responsibility of the funders: The document emphasizes the responsibility of the infrastructure facilities, senior researchers and research managers. Without funding, it will hardly be possible to speak of sustainability of research software. Funders can help achieving this by requesting software management plans and promoting maintenance and enhancement of research software. The role and the responsibility of the funders is yet missing in the document.

* Specifying the career opportunities for software developers in research: The document points out the problem, but without proper solutions. The current [collective agreement for public sector](https://www.tdl-online.de/fileadmin/downloads/rechte_Navigation/A._TV-L__2011_/01_Tarifvertrag/Aenderungstarifvertrag_Nr._5_zum_TV-L.pdf) (TV-L) provides for the employees in the programming salary classifications between E8 and E11 – from 40 T€ to 55 T€ p.a. before tax. With these numbers, it is hard for the public sector to become an attractive IT-employer and compete with private sector which offer much higher salaries for software developers. The suggestion of helping to recognize software development as an academic achievement and as a career path for scientists is a well-intentioned one, but probably not as effective in solving the problem as raising the salaries and creating interesting IT-employers out of data centers and libraries. Research software engineering can be a part of the service portfolio of the data centers and libraries in the near future.

Having said that, the Recommendations on the Development, Use and Provision of Research Software is a very useful document for the research community to start a long overdue discussion about the sustainability of the research software.
