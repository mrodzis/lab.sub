# lab.sub

## new authors
please register at [`_data/authors.yml`](_data/authors.yml)

## new posts
create a markdown file in the `_posts` directory. you have to use the filename
template `yyyy-mm-dd-title.md`. the `title` part will be resource name in the
url `/title.html`.

in the markdown file you have to use a front matter (header) like in the
[welcome.md](_posts/2017-01-01-welcome.md).

for customized styles you can edit the `subclass` value.

## tags
Everyone is free to define new tags. All tags visible via `tags.html`. Further
specifications of a tag may be added to [data file](_data/(tags.yml)). The cover
image is optional but if not specified a file with tag-TAGNAME.png is used if
present.

## attachments
You can add attachments to your posts. Just add `attachments: [comma separated file names]`
to the front matter of your post and upload the files to the `attachments` directory.

## local preview
```bash
bundle install
bundle exec jekyll serve
```

## customizations
### CSS
please add minified CSS only. add complete stylesheets to `assets/css/` or add
yours to the end of [`screen.css`](assets/css/screen.css)

call `gulp` to minify and stage the files with `git add assets/built/*`.

### images
add your images to `assets/images` and always minify them with `gulp` as well.

## template
Jasper2: http://jekyllthemes.org/themes/jasper2/

### fork
from: https://github.com/myJekyll/jasper2

to: https://gitlab.gwdg.de/subugoe/lab.sub
